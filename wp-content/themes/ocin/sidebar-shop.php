<?php
if ( is_active_sidebar( 'shop-sidebar' ) && ! is_single() ) { 
?>
<?php 
$ocin_shop_sidebar_position = get_theme_mod( 'ocin_shop_sidebar_position', 'top' );
if ( 'aside' == $ocin_shop_sidebar_position || isset( $_GET[ 'sidebar_side' ] ) ) {
    $ocin_sidebar_class = 'col-md-3 col-md-pull-9';
}else{
    $ocin_sidebar_class = 'col-md-12';
}
?>
    <aside id="sidebar" class="woocommerce-sidebar <?php echo $ocin_sidebar_class; ?>">

        <?php
		if (function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'shop-sidebar' ) ) : else :

        endif;
        ?>
 
        <div class="clearfix"></div>
	</aside>
<?php }//if is_active_sidebar ?> 
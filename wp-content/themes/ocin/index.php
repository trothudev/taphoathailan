<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ocin
 */

get_header(); ?>

	<?php
	$ocin_blog_layout = get_theme_mod( 'ocin_blog_layout', 'masonry' );
    $content_class = 'col-md-8';
    $col_class = '';
    if (  is_home() && 'masonry' == $ocin_blog_layout && ! isset( $_GET[ 'blog_layout' ] ) ) {
        $col_class = 'blog_masonry';
        $content_class = '';
    }
	?>
	<div id="content" class="<?php echo esc_attr( $content_class ); ?>">

		<?php if ( have_posts() ) : ?>

			<div id="sub-content" class="<?php echo esc_attr( $col_class ); ?>">

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );
					?>

				<?php endwhile; ?>

			</div><!-- /blog_masonry_posts -->

			<?php get_template_part( 'template-parts/pagination', 'index' ); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

	</div><!-- /content -->


	<?php
	if (  'normal' == $ocin_blog_layout || isset( $_GET[ 'blog_layout' ] ) ) {
		get_sidebar();
	}
	?>


<?php get_footer(); ?>

<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ocin
 */

?>

        </main><!-- #main -->

    </div><!-- /#container -->

	<div class="sub-footer">
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-sm-6">
                    
                    <p>&copy; Copyright <?php echo date('Y'); ?> <a rel="nofollow" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html( bloginfo( 'name' ) ); ?></a></p>

                    <?php
                    if ( has_nav_menu( 'footer-menu' ) ) {
                        wp_nav_menu(
                            array(
                                'theme_location'  => 'footer-menu',
                                'container'       => 'div',
                                'container_id'    => 'footer-menu',
                                'container_class' => 'menu',
                                'menu_id'         => 'menu-footer-items',
                                'menu_class'      => 'menu-items',
                                'depth'           => 1,
                                'fallback_cb'     => '',
                            )
                        );
                    }
                    ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?php get_template_part( '/template-parts/social-menu', 'footer' ); ?>
                </div>

            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .sub-footer -->


<?php wp_footer(); ?>

</body>
</html>
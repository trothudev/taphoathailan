var $container_isotope, args_isotope;
(function($) {
  "use strict";

jQuery(document).ready(function($) {

			
			//Anomation at load -----------------
			Pace.on('done', function(event) {
				
			});//Pace
			
			//WooCommerce Sidebar
			$(".sidebar_btn").on('click', function(event) {
				event.preventDefault();
				$(this).toggleClass('open');
				$(".woocommerce-sidebar").slideToggle().toggleClass('open');
			});

			//Minimal Menu
			$(".minimal-btn").on('click', function(event) {
				event.preventDefault();
				$(this).toggleClass('open');
				$(".bottom-menu").slideToggle().toggleClass('open');
			});

			/*
			WooCommerce Cart Widget Button
			=========================================================
			*/
			$('body').on('click', '.ql_cart-btn', function(event) {
				event.preventDefault();
				/* Act on the event */
				window.location.href = $(this).attr('href');
			});

			/*
			WooCommerce Cart Widget
			=========================================================
			*/
			function createCarousel(){
				$("#ql_woo_cart .products").owlCarousel({
				    loop: false,
				    margin: 10,
				    dots: false,
				    responsiveClass: true,
				    responsive: {
				        0:{
				            items: 1
				        },
				        760:{
				            items: 3
				        },
				        1306:{
				            items: 5
				        },
				        1490:{
				            items: 6
				        }
				    }
				});
			}
			createCarousel();

			$( 'body' ).on('click', '#ql_woo_cart .owl-next', function(event) {
				event.preventDefault();
				$("#ql_woo_cart .products").trigger('next.owl.carousel');
			});
			$( 'body' ).on('click', '#ql_woo_cart .owl-prev', function(event) {
				event.preventDefault();
				$("#ql_woo_cart .products").trigger('prev.owl.carousel');
			});
			//Updates Owl Carouse after the mini cart is updated
			$(document.body).bind("added_to_cart",function(){
				$("#ql_woo_cart .products").trigger('destroy.owl.carousel');
			  	createCarousel();
			});
			//Updates Carousel on the first load of the page
			$(document.body).bind("wc_fragments_refreshed",function(){
				$("#ql_woo_cart .products").trigger('destroy.owl.carousel');
			  	createCarousel();
			});


			


			/*
			WooCommerce Masonry
			=========================================================
			*/
			$container_isotope = $('#main .products');
			//Add preloader
			$container_isotope.before('<div class="preloader"><i class="fa fa-cog fa-spin"></i></div>');

			//Isotope parameters
			args_isotope = {
				itemSelector : '.product',
				layoutMode : 'packery',
			    percentPosition: true,
			    transitionDuration: 0
			};
			
			//Wait to images load
			$container_isotope.imagesLoaded(  function( $images, $proper, $broken ) {

				if ( $container_isotope.hasClass( 'masonry' ) ) {
					//Start Isotope
					$container_isotope.isotope( args_isotope );
				};

				//Remove preloader
				$container_isotope.prev('.preloader').addClass('proloader_hide');
						
			});//images loaded

			//Infinite Scroll
			$(window).scroll(function() {
				if( $(window).scrollTop() + window.innerHeight == $(document).height() ) {
			   		if ( $container_isotope.hasClass( 'infinite-scroll' ) ) {
			    		$( "#ql_load_more" ).trigger( "click" );
			   		}
				}
			});


			




			/*
			AJAX Shop Categories
			=========================================================
			*/
			$('body').on('click', '.ql_woocommerce_categories a', function(event) {
				
				if ( $container_isotope.hasClass( 'masonry' ) ) { //AJAX only works with masonry

					event.preventDefault();

					//If we are in a category archive, don't do ajax
					if ( $('body').hasClass('tax-product_cat') ) {
						window.location.href = $(this).attr('href');
					}else{

						$('.ql_woocommerce_categories li').removeClass('current');
						$(this).parent('li').addClass('current');

						var category = $(this).attr( 'data-category' );
						$container_isotope.prev('.preloader').removeClass('proloader_hide');
						$container_isotope.addClass('products_hide');

						//Add correct URL to browser
						var $new_url = $(this).attr('href');
						document.title = capitalizeFirstLetter(category);
					    window.history.pushState(category, category, $new_url);

						$.ajax({
							url: ocin.admin_ajax,
							type: 'POST',
							dataType: 'html',
							data: {
								action: 'ocin_load_items',
								token: ocin.token,
								category: category
							},
						})
						.done(function(data) {
							
							// create new item elements
							var $items = $(data);
							var $current_items = $container_isotope.isotope('getItemElements');
							$container_isotope.isotope( 'remove', $current_items );//remove old items

							// Insert items to grid
							$container_isotope.isotope( 'insert', $items );

							// layout Isotope after each image loads
							$container_isotope.imagesLoaded().progress( function() {
								$container_isotope.isotope('layout');
							});
							$('.woocommerce-result-count').hide();

							$('#ql_load_more').show();

							//Remove preloader and show items
							setTimeout(function(){
								$container_isotope.prev('.preloader').addClass('proloader_hide');
								$container_isotope.removeClass('products_hide');
							}, 1000);

						})
						.fail(function() {
							//If AJAX don't work just go to the href
							window.location.href = $(this).attr('href');
						});

					}//if $('body').hasClass('tax-product_cat')
				};

			});


			/*
			AJAX Shop Load More
			=========================================================
			*/
			$('body').on('click', '#ql_load_more', function(event) {
				event.preventDefault();

				var $this = $(this);
				$this.addClass('loading_items');
				var category = $('.ql_woocommerce_categories .current a').attr( 'data-category' );
				var offset = $container_isotope.isotope('getItemElements').length;
				$.ajax({
					url: ocin.admin_ajax,
					type: 'POST',
					dataType: 'html',
					data: {
						action: 'ocin_load_items',
						token: ocin.token,
						category: category,
						offset: offset
					},
				})
				.done(function(data) {
					if ( data.length > 0 ) {
						// create new item elements
						var $items = $(data);
					
						$items.addClass('product_added');
						// Insert items to grid
						$container_isotope.isotope( 'insert', $items );

						// layout Isotope after each image loads
						$container_isotope.imagesLoaded().progress( function() {
							
							$container_isotope.isotope('layout');
							
						});
						$items.removeClass('product_added');
						$this.removeClass('loading_items');

					}else{
						$('#ql_load_more').hide();
						$this.removeClass('loading_items');
					}

				})
				.fail(function() {

				});
			});





			/*
			// WooCommerce Carousel
			//===========================================================
			*/
			//Carousel for Single WooCommerce images
			var ql_owl_woo = $('.ql_main_images');
			ql_owl_woo.owlCarousel({
			    center: true,
			    items: 1,
			    loop: false,
			    dots: false,
			    margin:10
			});

			var $ql_woo_thumbnails = $(".ql_thumbnail_column a");
			$ql_woo_thumbnails.eq(0).addClass("current"); //Make first thumbnail active
			//Change thumbnails state on slider change
			ql_owl_woo.on('changed.owl.carousel', function(event) {
				var item = event.item.index;
				$ql_woo_thumbnails.removeClass("current");
				$ql_woo_thumbnails.eq(item).addClass("current");
			})
			//WooCommerce thumbnails
			$ql_woo_thumbnails.on('click', function(event) {
				event.preventDefault();
			});
			$ql_woo_thumbnails.hover(function() {				
				$ql_woo_thumbnails.removeClass("current");
				$(this).addClass("current");
				ql_owl_woo.trigger('to.owl.carousel', [$(this).index(), false, true]);
			});
			//Prev and Next buttons
			$(".ql_main_images_btn.ql_next").on('click', function(event) {
				event.preventDefault();
				ql_owl_woo.trigger('next.owl.carousel');
			});
			$(".ql_main_images_btn.ql_prev").on('click', function(event) {
				event.preventDefault();
				ql_owl_woo.trigger('prev.owl.carousel');
			});
			//PhotoSwipe for WooCommerce Images
			initPhotoSwipe('.ql_main_images', 'img', ql_owl_woo);
			/*			
			//===========================================================
			*/

			/*
			// WooCommerce Custom Single Add to cart button
			//===========================================================
			*/
			$( ".summary-bottom .quantity .qty" ).change(function() {
				$( ".entry .quantity .qty" ).val( $(this).val() );
			});
			$( 'body' ).on('click', '.summary-bottom .single_add_to_cart_button', function(event) {
				$( ".entry .single_add_to_cart_button" ).click();
			});


			/*
			// WooCommerce Custom Variations
			//===========================================================
			*/
			$(".variations").after('<div class="ql_custom_variations"></div>');
			//Create custom variations
			$(".variations tr").each(function(index, el) {
				var $select = $(this).find("select");
				var ul = $("<ul></ul>");
				var div_variation = $('<div class="ql_custom_variation"></div>');
				var select_id = $select.attr('id');

				ul.attr('id', 'ql_' + select_id);
				ul.attr('class', 'ql_select_id');

				//If the variation is color
				if (select_id.indexOf("color") > -1) {
					ul.addClass("ql_color_variation");
				};

				$select.find('option').each(function(index_option, el_option) {
					var current_value = $(this).attr('value');
					if (current_value != '') {
						var li = $("<li></li>");
						var a = $("<a href='#'></a>");
						
						a.attr('data-value',  current_value );
						a.text($(this).text());
						//If the variation is color
						if (select_id.indexOf("color") > -1) {
							a.prepend($('<i></i>').css('background-color', current_value).addClass("ql_" + current_value));
						};
						li.append(a);
						ul.append(li);
					};
				});
				div_variation.append($("<h5></h5>").text($(el).find(".label").text()));
				div_variation.append(ul);
				$(".ql_custom_variations").append(div_variation);
			});
			$('body').on('click', ".ql_custom_variation ul li a", function(event) {
				event.preventDefault();
				var option_val = $(this).attr('data-value');
				var slect_id = $(this).parents(".ql_select_id").attr('id');
				slect_id = slect_id.replace("ql_", "");
				$("#"+slect_id + ' option').each(function(index, el) {
					$(el).removeAttr('selected');
				});
				//$("#"+slect_id + ' option[value="' + option_val + '"]').prop('selected', true).attr('selected', 'selected'); //Old search
				jQuery("#"+slect_id + ' option').filter(function(){return this.value==option_val}).prop('selected', true).attr('selected', 'selected'); //Better search when there are HTML chars
				//$("#"+slect_id).val(option_val);
				$("#"+slect_id).change();

				$(this).parents(".ql_select_id").find("a").removeClass("current");
				$(this).addClass("current");
			});
			/*			
			//===========================================================
			*/

			/*
			Goes to first Image in Single Carousel when variation change
			=========================================================
			*/
			$( ".variations_form" ).on( 'woocommerce_variation_select_change', function() {
				console.log("Imagen CAmbiada");
				setTimeout(function(){ 
					ql_owl_woo.trigger('to.owl.carousel', [0, false, true]);
				}, 200);
			} );
			/*			
			//===========================================================
			*/




			/*
			Blog Masonry
			=========================================================
			*/
			var $blog_isotope = $('.blog_masonry');
			//Add preloader
			$blog_isotope.append('<div class="preloader"><i class="fa fa-cog fa-spin"></i></div>');

			//Isotope parameters
			var blog_args_isotope = {
				itemSelector : '.post',
				layoutMode : 'packery',
			    percentPosition: true
			};
			
			//Wait to images load
			$blog_isotope.imagesLoaded(  function( $images, $proper, $broken ) {

				//Start Isotope
				$blog_isotope.isotope( blog_args_isotope );

				//Remove preloader
				$blog_isotope.find('.preloader i').css('display', 'none');
				$blog_isotope.children('.preloader').css('opacity', 0).delay(900).fadeOut();
						
			});//images loaded
			/*			
			//===========================================================
			*/






			$(".ql_scroll_top").click(function() {
			  $("html, body").animate({ scrollTop: 0 }, "slow");
			  return false;
			});

			$("#primary-menu > li > ul > li.dropdown").each(function(index, el) {
				$(el).removeClass('dropdown').addClass('dropdown-submenu');
			});


			
			$('.dropdown-toggle').dropdown();

			$('*[data-toggle="tooltip"]').tooltip();
			

			//Sidebar Menu Function
			$('#sidebar .widget ul:not(.product-categories) li ul').parent().addClass('hasChildren').append("<i class='fa fa-angle-down'></i>");
			var children;
			$("#sidebar .widget ul:not(.product-categories) li").hoverIntent(
				function () {
					children = $(this).children("ul");
					if($(children).length > 0){ $(children).stop(true, true).slideDown('fast'); }
				}, 
				function () {
				  $(this).children('ul').stop(true, true).slideUp(500);
				}
			);
			//Footer Menu Function
			$('footer .widget ul:not(.product-categories) li ul').parent().addClass('hasChildren').append("<i class='fa fa-angle-down'></i>");
			var children;
			$("footer .widget ul:not(.product-categories) li").hoverIntent(
				function () {
					children = $(this).children("ul");
					if($(children).length > 0){ $(children).stop(true, true).slideDown('fast'); }
				}, 
				function () {
				  $(this).children('ul').stop(true, true).slideUp(500);
				}
			);	

									

});//DOM ready
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
})(jQuery);
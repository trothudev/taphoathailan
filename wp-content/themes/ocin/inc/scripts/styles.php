<?php

/**
 * Enqueues front-end CSS for color scheme.
 *
 * @see wp_add_inline_style()
 */
function ocin_custom_css() {
	/*
	Colors
	*/
	$heroColor = get_theme_mod( 'ocin_hero_color', '#00b09a' );
	$headings_color = get_theme_mod( 'ocin_headings_color', '#444444' );
	$text_color = get_theme_mod( 'ocin_text_color', '#777777' );
	$link_color = get_theme_mod( 'ocin_link_color', '#00b09a' );
	$content_background_color = get_theme_mod( 'ocin_content_background_color', '#FFFFFF' );

	$colors = array(
		'heroColor'      => $heroColor,
		'headings_color' => $headings_color,
		'text_color'     => $text_color,
		'link_color'     => $link_color,
		'content_background_color'     => $content_background_color,
	);

	$custom_css = ocin_get_custom_css( $colors );

	wp_add_inline_style( 'ocin-style', $custom_css );


	/*
	Typography
	*/
	$ocin_typography_font_family = get_theme_mod( 'ocin_typography_font_family', 'Lato' );
	$ocin_typography_subsets = get_theme_mod( 'ocin_typography_subsets', '' );
	$ocin_typography_font_weight = get_theme_mod( 'ocin_typography_font_weight', '400' );
	$ocin_typography_font_size = get_theme_mod( 'ocin_typography_font_size', '16' );

	$typography = array(
		'font-family'      => $ocin_typography_font_family,
		'font-weight'     => $ocin_typography_font_weight,
		'font-size'     => $ocin_typography_font_size,
	);

	//Add Google Fonts
	$ocin_font_subset = '';
	if ( is_array( $ocin_typography_subsets ) ) {
		$ocin_font_subset = '&subset=';
		foreach ( $ocin_typography_subsets as $subset ) {
			$ocin_font_subset .= $subset . ',';
		}
		$ocin_font_subset = rtrim( $ocin_font_subset, ',' );
	}

	$ocin_google_font = '//fonts.googleapis.com/css?family=' . $ocin_typography_font_family . ':' . $ocin_typography_font_weight . ',700' . $ocin_font_subset;
	wp_enqueue_style( 'ocin_google-font', $ocin_google_font, array(), '1.0', 'all');

	$custom_css = ocin_get_custom_typography_css( $typography );

	wp_add_inline_style( 'ocin-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'ocin_custom_css' );



/**
 * Returns CSS for the color schemes.
 *
 * @param array $colors colors.
 * @return string CSS.
 */
function ocin_get_custom_css( $colors ) {

	//Default colors
	$colors = wp_parse_args( $colors, array(
		'heroColor'            => '#00b09a',
		'headings_color'       => '#444444',
		'text_color'           => '#777777',
		'link_color'           => '#00b09a',
		'content_background_color'           => '#FFFFFF',
	) );

	$css_variations = '.woocommerce #main .entry-summary .variations';
	$ocin_shop_variations = get_theme_mod( 'ocin_shop_variations', '1' );

	if ( '2' == $ocin_shop_variations ) {
		$css_variations = '.ql_custom_variations';
	}

	$ocin_typography_font_size = get_theme_mod( 'ocin_typography_font_size', '16' );

	$css = <<<CSS
	html{
		font-size: {$ocin_typography_font_size}px;
	}
	/* Text Color */
	body{
		color: {$colors['text_color']};
	}
	h1, h2, h3, h4, h5, h6, h1 a, h2, a, h3 a, h4 a, h5 a, h6 a{
		color: {$colors['headings_color']};
	}
	/* Link Color */
	a{
		color: {$colors['link_color']};
	}



	/*============================================
	// Featured Color
	============================================*/

	/* Background Color */
	.btn-ql,
	.pagination li.active a,
	.pagination li.active a:hover,
	.wpb_wrapper .products .product-category h3,
	.btn-ql:active,
	.btn-ql.alternative:hover,
	.btn-ql.alternative-white:hover,
	.btn-ql.alternative-gray:hover,
	.hero_bck,
	.ql_nav_btn:hover,
	.ql_nav_btn:active,
	.cd-popular .cd-select,
	.no-touch .cd-popular .cd-select:hover,
	.pace .pace-progress,
	.woocommerce .products .product .add_to_cart_button:hover,
	#ql_woo_cart .widget_shopping_cart_content a.button.checkout,
	.woocommerce #main .single_add_to_cart_button,
	.woocommerce .woocommerce-MyAccount-navigation ul .woocommerce-MyAccount-navigation-link.is-active a
	{
		background-color: {$colors['heroColor']};
	}
	/* Border Color */
	.btn-ql,
	.pagination li.active a,
	.pagination li.active a:hover,
	.btn-ql:active,
	.btn-ql.alternative,
	.btn-ql.alternative:hover,
	.btn-ql.alternative-white:hover,
	.btn-ql.alternative-gray:hover,
	.hero_border,
	.pace .pace-activity,
	#ql_woo_cart .widget_shopping_cart_content a.button.checkout,
	.woocommerce #main .single_add_to_cart_button
	{
		border-color: {$colors['heroColor']};
	}
	/* Color */
	.pagination .current,
	.pagination a:hover,
	.widget_recent_posts ul li h6 a, .widget_popular_posts ul li h6 a,
	.read-more,
	.read-more i,
	.btn-ql.alternative,
	.hero_color,
	.cd-popular .cd-pricing-header,
	.cd-popular .cd-currency, .cd-popular .cd-duration,
	#sidebar .widget ul li > a:hover,
	#sidebar .widget_recent_comments ul li a:hover,
	#ql_woo_cart .widget_shopping_cart_content a.button.checkout:hover,
	.woocommerce #main .single_add_to_cart_button:hover,
	.woocommerce #main .price
	{
		color: {$colors['heroColor']};
	}

	/*============================================
	// Content Background Color
	============================================*/
	.ql_background_padding,
	.product_text,
	.blog article, .search article, .archive article,
	.woocommerce .product .summary .summary-top,
	.woocommerce .product .summary .entry,
	.woocommerce .product .summary .summary-bottom,
	.woocommerce div.product .woocommerce-tabs .panel,
	.woocommerce div.product .woocommerce-tabs ul.tabs li,
	.woocommerce div.product .woocommerce-tabs ul.tabs li.active,
	#ql_load_more
	{
		background-color: {$colors['content_background_color']};
	}


	/*============================================
	// Hide Variations
	============================================*/
	{$css_variations}{
		display: none;
	}

CSS;

	return $css;
}


/**
 * Returns CSS for the typography styles.
 *
 * @param array $typography typography.
 * @return string CSS.
 */
function ocin_get_custom_typography_css( $typography  ) {

	//Default colors
	$typography = wp_parse_args( $typography, array(
		'font-family'      => 'Lato',
		'font-weight'     => '400',
		'font-size'     => '16',
	) );

	$css = <<<CSS

	/* Typography */
	body{
		font-family: {$typography['font-family']};
		font-weight: {$typography['font-weight']};
		font-size: {$typography['font-size']}px;
	}

CSS;

	return $css;
}

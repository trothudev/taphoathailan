<?php
	//Bootstrap =======================================================
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.1', 'all');
	//=================================================================

	//Owl Carousel =======================================================
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '1.1.1', 'all');
	//=================================================================

	//Photoswipe ======================================================
	wp_register_style( 'photoswipe', get_template_directory_uri() . '/css/photoswipe.css', array(), '2.0.0', 'all' );  
	wp_enqueue_style( 'photoswipe' );  
	//=================================================================

	//Photoswipe Skin ======================================================
	wp_register_style( 'photoswipe-skin', get_template_directory_uri() . '/css/default-skin/default-skin.css', array(), '2.0.0', 'all' );  
	wp_enqueue_style( 'photoswipe-skin' );  
	//=================================================================


	wp_enqueue_style( 'ocin-style', get_stylesheet_uri(), array( 'bootstrap', 'owl-carousel', 'photoswipe', 'photoswipe-skin' ), wp_get_theme()->get('Version') );


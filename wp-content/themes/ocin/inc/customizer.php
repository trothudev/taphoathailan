<?php
/**
 * Ocin Theme Customizer.
 *
 * @package Ocin
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function ocin_customize_register( $wp_customize ) {


	/**
	 * Control for the PRO buttons
	 */
	class ocin_Pro_Version extends WP_Customize_Control{
		public function render_content()
		{
			$args = array(
				'a' => array(
					'href' => array(),
					'title' => array()
					),
				'br' => array(),
				'em' => array(),
				'strong' => array(),
				);
			echo wp_kses( $this->label, $args );
		}
	}

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';


	/*
	Logo
	------------------------------ */
	$wp_customize->add_setting( 'ocin_logo', array( 'default' => '', 'transport' => 'postMessage', 'sanitize_callback' => 'attachment_url_to_postid', ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ocin_logo', array(
        'label'    => esc_attr__( 'Logo', 'ocin' ),
        'section'  => 'title_tagline',
        'settings' => 'ocin_logo',
        'priority' => 5
    ) ) );


	/*
    Colors
    ===================================================== */
    	/*
		Featured
		------------------------------ */
		$wp_customize->add_setting( 'ocin_hero_color', array( 'default' => '#0a9878', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_hero_color', array(
			'label'        => esc_attr__( 'Featured Color', 'ocin' ),
			'section'    => 'colors',
		) ) );

		/*
		Headings
		------------------------------ */
		$wp_customize->add_setting( 'ocin_headings_color', array( 'default' => '#444444', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_headings_color', array(
			'label'        => esc_attr__( 'Headings Color', 'ocin' ),
			'section'    => 'colors',
		) ) );

		/*
		Text
		------------------------------ */
		$wp_customize->add_setting( 'ocin_text_color', array( 'default' => '#777777', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_text_color', array(
			'label'        => esc_attr__( 'Text Color', 'ocin' ),
			'section'    => 'colors',
		) ) );

		/*
		Link
		------------------------------ */
		$wp_customize->add_setting( 'ocin_link_color', array( 'default' => '#424242', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_link_color', array(
			'label'        => esc_attr__( 'Link Color', 'ocin' ),
			'section'    => 'colors',
		) ) );

		/*
		Content Background Color
		------------------------------ */
		$wp_customize->add_setting( 'ocin_content_background_color', array( 'default' => '#FFFFFF', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_content_background_color', array(
			'label'        => esc_attr__( 'Content Background Color', 'ocin' ),
			'section'    => 'colors',
		) ) );


	/*
    Header Options
    ===================================================== */
	$wp_customize->add_section( 'ocin_header_options_section', array(
			'title' => esc_attr__( 'Header Options', 'ocin' ),
			'priority' => 50,
	) );

	$header_layouts = array(
			'default' => esc_attr__( 'Default', 'ocin' ),
			'menu-center' => esc_attr__( 'Menu Centered', 'ocin' ),
			'minimal' => esc_attr__( 'Minimal', 'ocin' ),
		);
	$wp_customize->add_setting( 'ocin_header_layout', array( 'default' => 'default', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
	$wp_customize->add_control( 'ocin_header_layout', array(
        'label'   => esc_attr__( 'Header Layout', 'ocin' ),
        'section' => 'ocin_header_options_section',
        'settings'   => 'ocin_header_layout',
        'type'       => 'select',
        'choices'    => $header_layouts,
    ));



	/*
	Shop Categories selection
	------------------------------ */
	if ( class_exists( 'WooCommerce' ) && class_exists( 'Kirki' ) ){

		$wp_customize->add_section( 'ocin_shop_options', array(
			'title' => esc_attr__( 'Shop Options', 'ocin' ),
			'priority' => 120,
		) );

		$all_categories = get_categories( array(
		         'taxonomy'     => 'product_cat',
		) );
		$woo_categories = array();
		if ( $all_categories ) {
			foreach ( $all_categories as $cat ) {
				$woo_categories[ esc_attr( $cat->slug ) ] = $cat->name;
			}
		}
	    Kirki::add_field( 'ocin_shop_categories', array(
		    'type'        => 'multicheck',
		    'settings'    => 'ocin_shop_categories',
		    'label'   => esc_attr__( 'Shop Categories', 'ocin' ),
		    'description' => esc_attr__( 'Select categories for the shop categories menu.', 'ocin' ),
		    'section'     => 'ocin_shop_options',
		    'choices' => $woo_categories,
		    'default'     => 12,
		) );

	    // $wp_customize->add_setting( 'ocin_shop_products_amount', array( 'default' => '12', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		// $wp_customize->add_control( 'ocin_shop_products_amount', array(
		// 	'type' => 'text',
		// 	'section' => 'ocin_shop_options', // Required, core or custom.
		// 	'label' => esc_attr__( "Number of products", 'ocin' ),
		// 	'description' => esc_attr__( 'Number of products displayed per page.', 'ocin' ),
		// ) );

	    Kirki::add_field( 'ocin_shop_products_amount', array(
		    'type'        => 'number',
		    'settings'    => 'ocin_shop_products_amount',
		    'label'       => esc_attr__( "Number of products", 'ocin' ),
		    'description' => esc_attr__( 'Number of products displayed per page.', 'ocin' ),
		    'section'     => 'ocin_shop_options',
		    'default'     => 12,
		) );

		Kirki::add_field( 'ocin_shop_layout', array(
		    'type'        => 'radio-image',
		    'transport'   => 'postMessage',
		    'settings'    => 'ocin_shop_layout',
		    'label'       => esc_attr__( 'Shop Layout', 'ocin' ),
		    'description' => esc_attr__( 'Select between a grid or masonry layout.', 'ocin' ),
		    'section'     => 'ocin_shop_options',
		    'default'     => 'grid',
		    'choices'     => array(
		        'grid'   => get_template_directory_uri() . '/images/grid_layout.png',
		        'masonry' => get_template_directory_uri() . '/images/masonry_layout.png',
		    ),
		) );

		$wp_customize->add_setting( 'ocin_shop_columns', array( 'default' => '4', 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_shop_columns', array(
	        'label'   => esc_attr__( 'Select Columns', 'ocin' ),
	        'section' => 'ocin_shop_options',
	        'settings'   => 'ocin_shop_columns',
	        'type'       => 'radio',
	        'choices'    => array(
	        	'5' => esc_attr__( '5 Columns', 'ocin' ),
	            '4' => esc_attr__( '4 Columns', 'ocin' ),
	        	'3' => esc_attr__( '3 Columns', 'ocin' ),
	        	'2' => esc_attr__( '2 Columns', 'ocin' )
	        ),
	        'description' => esc_attr__( 'How many columns for the shop layout?.', 'ocin' ),
	        'priority' => 50
	    ));

	    Kirki::add_field( 'ocin_shop_page_layout', array(
		    'type'        => 'radio-image',
		    'transport'   => 'postMessage',
		    'settings'    => 'ocin_shop_page_layout',
		    'label'       => esc_attr__( 'Shop Page Layout', 'ocin' ),
		    'description' => esc_attr__( 'Choose from a narrow or full width shop page.', 'ocin' ),
		    'section'     => 'ocin_shop_options',
		    'default'     => 'shop-narrow',
		    'choices'     => array(
		        'shop-narrow'   => get_template_directory_uri() . '/images/shop-narrow.png',
		        'shop-fullwidth' => get_template_directory_uri() . '/images/shop-fullwidth.png',
		    ),
		) );

	    Kirki::add_field( 'ocin_shop_sidebar_position', array(
		    'type'        => 'radio-image',
		    'settings'    => 'ocin_shop_sidebar_position',
		    'label'       => esc_attr__( 'Sidebar Position', 'ocin' ),
		    'description' => esc_attr__( 'Select where you want the Sidebar', 'ocin' ),
		    'section'     => 'ocin_shop_options',
		    'default'     => 'top',
		    'choices'     => array(
		        'top'   => get_template_directory_uri() . '/images/sidebar_top.png',
		        'aside' => get_template_directory_uri() . '/images/sidebar_aside.png',
		    ),
		) );

		$wp_customize->add_setting( 'ocin_shop_infinitescroll_enable', array( 'default' => true, 'sanitize_callback' => 'ocin_sanitize_bool', 'type' => 'theme_mod' ) );
	    $wp_customize->add_control( 'ocin_shop_infinitescroll_enable', array(
			'section' => 'ocin_shop_options', // Required, core or custom.
			'label' => esc_attr__( "Infinite Scroll?", 'ocin' ),
			'description' => esc_attr__( 'Select if you want Infinite Scroll on Shop page', 'ocin' ),
			'type'    => 'checkbox',
			'priority' => 80
		) );

		$wp_customize->add_setting( 'ocin_shop_variations', array( 'default' => '1', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_shop_variations', array(
	        'label'   => esc_attr__( 'Select Variations', 'ocin' ),
	        'section' => 'ocin_shop_options',
	        'settings'   => 'ocin_shop_variations',
	        'type'       => 'radio',
	        'choices'    => array(
	            '1' => esc_attr__( 'Custom Variations', 'ocin' ),
	        	'2' => esc_attr__( 'Default Variations', 'ocin' )
	        ),
	        'description' => esc_attr__( 'If you use complex variations for your products, is better to use the default ones.', 'ocin' ),
	        'priority' => 90
	    ));

	}//if plugin exists

	/*
    Top Bar
    ===================================================== */
	$wp_customize->add_section( 'ocin_topbar_section', array(
			'title' => esc_attr__( 'Top Bar Options', 'ocin' ),
			'priority' => 120,
	) );

	$wp_customize->add_setting( 'ocin_topbar_enable', array( 'default' => true, 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_bool', 'type' => 'theme_mod' ) );
    $wp_customize->add_control( 'ocin_topbar_enable', array(
		'section' => 'ocin_topbar_section', // Required, core or custom.
		'label' => esc_attr__( "Show Top Bar?", 'ocin' ),
		'type'    => 'checkbox',
	) );

	$wp_customize->add_setting( 'ocin_topbar_text', array( 'default' => '', 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text_html', ) );
	$wp_customize->add_control( 'ocin_topbar_text', array(
		'type' => 'textarea',
		'section' => 'ocin_topbar_section', // Required, core or custom.
		'label' => esc_attr__( 'Text', 'ocin' ),
	) );

	$wp_customize->add_setting( 'ocin_topbar_color', array( 'default' => '#383838', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_topbar_color', array(
		'label'        => esc_attr__( 'Top Bar Color', 'ocin' ),
		'section'    => 'ocin_topbar_section',
	) ) );

	$wp_customize->add_setting( 'ocin_topbar_menu', array( 'default' => '', 'sanitize_callback' => 'ocin_sanitize_text', ) );
	$wp_customize->add_control( new ocin_Display_Text_Control( $wp_customize, 'ocin_topbar_menu', array(
		'section' => 'ocin_topbar_section', // Required, core or custom.
		'label' => sprintf( esc_html__( 'To add a menu go to: %s Appearance -> Menus %s And use the "Top Bar" location.', 'ocin' ), '<br><a href="' . get_admin_url( null, 'nav-menus.php' ). '">', '</a>. <br>' ),
	) ) );



	/*
    Blog Options
    ===================================================== */
	$wp_customize->add_section( 'ocin_blog_section', array(
			'title' => esc_attr__( 'Blog Options', 'ocin' ),
			'priority' => 130,
	) );

	$blog_layouts = array(
			'normal' => esc_attr__( 'Normal', 'ocin' ),
			'masonry' => esc_attr__( 'Masonry', 'ocin' ),
		);
	$wp_customize->add_setting( 'ocin_blog_layout', array( 'default' => 'masonry', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
	$wp_customize->add_control( 'ocin_blog_layout', array(
        'label'   => esc_attr__( 'Blog Layout', 'ocin' ),
        'section' => 'ocin_blog_section',
        'settings'   => 'ocin_blog_layout',
        'type'       => 'select',
        'choices'    => $blog_layouts,
    ));





	/*
    Slider Panel
    ===================================================== */
    $wp_customize->add_panel( 'ocin_slider_panel', array(
		'title' => esc_attr__( 'Slider Section', 'ocin' ),
		'description' => '', // Include html tags such as <p>.
		'priority' => 160
	) );
		/*
    	Slider
    	------------------------------ */
    	if ( class_exists( 'MetaSliderPlugin' ) ){
    		$ocin_meta_slider_url = 'admin.php?page=metaslider';
    	}else{
    		$ocin_meta_slider_url = 'themes.php?page=tgmpa-install-plugins';
    	}
    	$wp_customize->add_section( 'ocin_slider_section', array(
			'title' => esc_attr__( 'Slider', 'ocin' ),
			'description' => sprintf( esc_html__( 'To add a Slider make sure you have installed the %s Meta Slider %s plugin.', 'ocin' ), '<a href="' . get_admin_url( null, $ocin_meta_slider_url ) .'">', '</a>' ),
			'panel' => 'ocin_slider_panel'
		) );

    	if ( class_exists( 'MetaSliderPlugin' ) ){
    		$sliders = ocin_all_meta_sliders( 'title' );
    	}
    	$sliders_choices = array();
    	if ( ! empty( $sliders ) ) {
    		foreach ($sliders as $slider) {
    			$sliders_choices[$slider['id']] = $slider['title'];
    		}
    	}
    	$sliders_choices['no'] = esc_attr__( 'No slider', 'ocin' );
    	
		$wp_customize->add_setting( 'ocin_slider_id', array( 'default' => 'no', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_slider_id', array(
	        'label'   => esc_attr__( 'Select Slider', 'ocin' ),
	        'section' => 'ocin_slider_section',
	        'settings'   => 'ocin_slider_id',
	        'type'       => 'select',
	        'choices'    => $sliders_choices,
	        'priority' => 50
	    ));

	    $wp_customize->add_setting( 'ocin_slider_fullscreen', array( 'default' => false, 'sanitize_callback' => 'ocin_sanitize_bool', 'type' => 'theme_mod' ) );
	    $wp_customize->add_control( 'ocin_slider_fullscreen', array(
			'section' => 'ocin_slider_section', // Required, core or custom.
			'label' => esc_attr__( "Do you want a fullscreen slider?", 'ocin' ),
			'settings' => 'ocin_slider_fullscreen',
			'type'    => 'checkbox',
			'priority' => 60
		) );

	    /*
    	Banner1
    	------------------------------ */
    	$wp_customize->add_section( 'ocin_banner1_section', array(
			'title' => esc_attr__( 'Banner 1', 'ocin' ),
			'panel' => 'ocin_slider_panel'
		) );

		$wp_customize->add_setting( 'ocin_banner1_image', array( 'default' => '', 'transport' => 'postMessage', 'sanitize_callback' => 'attachment_url_to_postid', 'type' => 'theme_mod' ) );
	    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ocin_banner1_image', array(
	        'label'    => esc_attr__( 'Background Image', 'ocin' ),
	        'section'  => 'ocin_banner1_section',
	        'settings' => 'ocin_banner1_image',
	        'description' => esc_attr__( 'Recommended size: 333x240px', 'ocin' ),
	    ) ) );

	    $wp_customize->add_setting( 'ocin_banner1_title', array( 'default' => esc_html__( 'Your Title', 'ocin' ), 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner1_title', array(
			'type' => 'text',
			'section' => 'ocin_banner1_section', // Required, core or custom.
			'label' => esc_attr__( 'Title', 'ocin' ),
		) );

		/* translators: Lorem ipsum text, It is not strictly necessary to translate. */
		$wp_customize->add_setting( 'ocin_banner1_text', array( 'default' => esc_html__( 'Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.', 'ocin' ), 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text_html', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner1_text', array(
			'type' => 'textarea',
			'section' => 'ocin_banner1_section', // Required, core or custom.
			'settings' => 'ocin_banner1_text',
			'label' => esc_attr__( 'Text', 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_banner1_link_title', array( 'default' => esc_html__( 'View More', 'ocin' ), 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner1_link_title', array(
			'type' => 'text',
			'section' => 'ocin_banner1_section', // Required, core or custom.
			'settings' => 'ocin_banner1_link_title',
			'label' => esc_attr__( "Button Title", 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_banner1_link_url', array( 'default' => '#', 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_url', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner1_link_url', array(
			'type' => 'url',
			'section' => 'ocin_banner1_section', // Required, core or custom.
			'settings' => 'ocin_banner1_link_url',
			'label' => esc_attr__( "Button URL", 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_banner1_background_color', array( 'default' => '#68beec', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_banner1_background_color', array(
			'label'        => esc_attr__( 'Background Color', 'ocin' ),
			'settings' => 'ocin_banner1_background_color',
			'section'    => 'ocin_banner1_section',
		) ) );

		$wp_customize->add_setting( 'ocin_banner1_enable', array( 'default' => true, 'sanitize_callback' => 'ocin_sanitize_bool', 'type' => 'theme_mod' ) );
	    $wp_customize->add_control( 'ocin_banner1_enable', array(
			'section' => 'ocin_banner1_section', // Required, core or custom.
			'label' => esc_attr__( "Use this banner?", 'ocin' ),
			'settings' => 'ocin_banner1_enable',
			'type'    => 'checkbox',
		) );


		/*
    	Banner 2
    	------------------------------ */
    	$wp_customize->add_section( 'ocin_banner2_section', array(
			'title' => esc_attr__( 'Banner 2', 'ocin' ),
			'panel' => 'ocin_slider_panel'
		) );

		$wp_customize->add_setting( 'ocin_banner2_image', array( 'default' => '', 'transport' => 'postMessage', 'sanitize_callback' => 'attachment_url_to_postid', 'type' => 'theme_mod' ) );
	    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ocin_banner2_image', array(
	        'label'    => esc_attr__( 'Background Image', 'ocin' ),
	        'section'  => 'ocin_banner2_section',
	        'settings' => 'ocin_banner2_image',
	        'description' => esc_attr__( 'Recommended size: 333x240px', 'ocin' ),
	    ) ) );

	    $wp_customize->add_setting( 'ocin_banner2_title', array( 'default' => esc_html__( 'Your Title', 'ocin' ), 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner2_title', array(
			'type' => 'text',
			'section' => 'ocin_banner2_section', // Required, core or custom.
			'label' => esc_attr__( 'Title', 'ocin' ),
		) );

		/* translators: Lorem ipsum text, It is not strictly necessary to translate. */
		$wp_customize->add_setting( 'ocin_banner2_text', array( 'default' => esc_html__( 'Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.', 'ocin' ), 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text_html', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner2_text', array(
			'type' => 'textarea',
			'section' => 'ocin_banner2_section', // Required, core or custom.
			'settings' => 'ocin_banner2_text',
			'label' => esc_attr__( 'Text', 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_banner2_link_title', array( 'default' => esc_html__( 'View More', 'ocin' ), 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner2_link_title', array(
			'type' => 'text',
			'section' => 'ocin_banner2_section', // Required, core or custom.
			'settings' => 'ocin_banner2_link_title',
			'label' => esc_attr__( "Button Title", 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_banner2_link_url', array( 'default' => '#', 'transport' => 'postMessage', 'sanitize_callback' => 'ocin_sanitize_url', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_banner2_link_url', array(
			'type' => 'url',
			'section' => 'ocin_banner2_section', // Required, core or custom.
			'settings' => 'ocin_banner2_link_url',
			'label' => esc_attr__( "Button URL", 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_banner2_background_color', array( 'default' => '#68beec', 'transport' => 'postMessage', 'sanitize_callback' => 'sanitize_hex_color', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ocin_banner2_background_color', array(
			'label'        => esc_attr__( 'Background Color', 'ocin' ),
			'settings' => 'ocin_banner2_background_color',
			'section'    => 'ocin_banner2_section',
		) ) );

		$wp_customize->add_setting( 'ocin_banner2_enable', array( 'default' => true, 'sanitize_callback' => 'ocin_sanitize_bool', 'type' => 'theme_mod' ) );
	    $wp_customize->add_control( 'ocin_banner2_enable', array(
			'section' => 'ocin_banner2_section', // Required, core or custom.
			'label' => esc_attr__( "Use this banner?", 'ocin' ),
			'settings' => 'ocin_banner2_enable',
			'type'    => 'checkbox',
		) );


		/*
    	Typography
    	------------------------------ */
    	$wp_customize->add_section( 'ocin_typography_section', array(
			'title' => esc_attr__( 'Typography', 'ocin' ),
		) );   

		if ( class_exists( 'Kirki' ) ){

			Kirki::add_field( 'ocin_typography_font_family', array(
			    'type'     => 'select',
			    'settings' => 'ocin_typography_font_family',
			    'label'    => esc_html__( 'Font Family', 'ocin' ),
			    'section'  => 'ocin_typography_section',
			    'default'  => 'Lato',
			    'priority' => 20,
			    'choices'  => Kirki_Fonts::get_font_choices(),
			    'output'   => array(
			        array(
			            'element'  => 'body',
			            'property' => 'font-family',
			        ),
			    ),
			) );

			Kirki::add_field( 'ocin_typography_subsets', array(
			    'type'        => 'multicheck',
			    'settings'    => 'ocin_typography_subsets',
			    'label'       => esc_html__( 'Google-Font subsets', 'ocin' ),
			    'description' => esc_html__( 'The subsets used from Google\'s API.', 'ocin' ),
			    'section'     => 'ocin_typography_section',
			    'default'     => '',
			    'priority'    => 22,
			    'choices'     => Kirki_Fonts::get_google_font_subsets(),
			    'output'      => array(
			        array(
			            'element'  => 'body',
			            'property' => 'font-subset',
			        ),
			    ),
			) );

			Kirki::add_field( 'ocin_typography_font_weight', array(
			    'type'     => 'slider',
			    'settings' => 'ocin_typography_font_weight',
			    'label'    => esc_html__( 'Font Weight', 'ocin' ),
			    'section'  => 'ocin_typography_section',
			    'default'  => 400,
			    'priority' => 24,
			    'choices'  => array(
			        'min'  => 100,
			        'max'  => 900,
			        'step' => 100,
			    ),
			    'output'   => array(
			        array(
			            'element'  => 'body',
			            'property' => 'font-weight',
			        ),
			    ),
			) );

			Kirki::add_field( 'ocin_typography_font_size', array(
			    'type'      => 'slider',
			    'settings'  => 'ocin_typography_font_size',
			    'label'     => esc_html__( 'Font Size', 'ocin' ),
			    'section'   => 'ocin_typography_section',
			    'default'   => 16,
			    'priority'  => 25,
			    'choices'   => array(
			        'min'   => 7,
			        'max'   => 48,
			        'step'  => 1,
			    ),
			    'output' => array(
			        array(
			            'element'  => 'html',
			            'property' => 'font-size',
			            'units'    => 'px',
			        ),
			    ),
			    'transport' => 'postMessage',
			    'js_vars'   => array(
			        array(
			            'element'  => 'html',
			            'function' => 'css',
			            'property' => 'font-size',
			            'units'    => 'px'
			        ),
			    ),
			) );
		}else{
			$wp_customize->add_setting( 'ocin_typography_not_kirki', array( 'default' => '', 'sanitize_callback' => 'ocin_sanitize_text', ) );
			$wp_customize->add_control( new ocin_Display_Text_Control( $wp_customize, 'ocin_typography_not_kirki', array(
				'section' => 'ocin_typography_section', // Required, core or custom.
				'label' => sprintf( esc_html__( 'To change typography make sure you have installed the %s Kirki Toolkit %s plugin.', 'ocin' ), '<a href="' . get_admin_url( null, 'themes.php?page=tgmpa-install-plugins' ) . '">', '</a>' ),
			) ) );
		}//if Kirki exists


		/*
    	Contact Page
    	------------------------------ */
		$wp_customize->add_section( 'ocin_map_section', array(
			'title' => esc_attr__( 'Contact Page', 'ocin' ),
			'description' => esc_attr__( "Display a map and your contact information. You'll have to create a page using the 'Contact' page template.", 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_map_key', array( 'default' => '', 'sanitize_callback' => 'ocin_sanitize_text', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_map_key', array(
			'type' => 'text',
			'section' => 'ocin_map_section', // Required, core or custom.
			'label' => esc_attr__( "Google Maps API Key", 'ocin' ),
			'description' => sprintf( esc_html__( "An API Key is required for Google Maps to work. %s Sign up for one here %s (it's free for small usage)", 'ocin' ), '<a href="https://developers.google.com/maps/documentation/javascript/get-api-key">', '</a>' ) 
		) );

		$wp_customize->add_setting( 'ocin_map_lat_long', array( 'default' => '40.725987, -74.002447', 'sanitize_callback' => 'ocin_sanitize_lat_long', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_map_lat_long', array(
			'type' => 'text',
			'section' => 'ocin_map_section', // Required, core or custom.
			'label' => esc_attr__( "Latitude and Longitude", 'ocin' ),
			'description' => esc_attr__( 'Comma separated. Example: 40.725987, -74.002447', 'ocin' )
		) );

		$wp_customize->add_setting( 'ocin_map_zoom', array( 'default' => '13', 'sanitize_callback' => 'ocin_sanitize_integer', 'type' => 'theme_mod' ) );
		$wp_customize->add_control( 'ocin_map_zoom', array(
			'type' => 'text',
			'section' => 'ocin_map_section', // Required, core or custom.
			'label' => esc_attr__( "Zoom", 'ocin' ),
		) );

		$wp_customize->add_setting( 'ocin_contact_form', array( 'default' => '', 'sanitize_callback' => 'ocin_sanitize_text', ) );
		$wp_customize->add_control( new ocin_Display_Text_Control( $wp_customize, 'ocin_contact_form', array(
			'section' => 'ocin_map_section', // Required, core or custom.
			'label' => sprintf( esc_html__( 'Add a contact form by activating the Contact Form module of %s Jetpack %s plugin.', 'ocin' ), '<a href="' . get_admin_url( null, 'admin.php?page=jetpack_modules' ) .'">', '</a>' ),
		) ) );






   




}
add_action( 'customize_register', 'ocin_customize_register' );











/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function ocin_customize_preview_js() {
	
	wp_register_script( 'ocin_customizer_preview', get_template_directory_uri() . '/js/customizer-preview.js', array( 'customize-preview' ), '20151024', true );
	wp_localize_script( 'ocin_customizer_preview', 'wp_customizer', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'theme_url' => get_template_directory_uri(),
		'site_name' => get_bloginfo( 'name' )
	));
	wp_enqueue_script( 'ocin_customizer_preview' );

}
add_action( 'customize_preview_init', 'ocin_customize_preview_js' );


/**
 * Load scripts on the Customizer not the Previewer (iframe)
 */
function ocin_customize_js() {
	
	wp_enqueue_script( 'ocin_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-controls' ), '20151024', true );

}
add_action( 'customize_controls_enqueue_scripts', 'ocin_customize_js' );










/*
Sanitize Callbacks
*/

/**
 * Sanitize for post's categories
 */
function ocin_sanitize_categories( $value ) {
    if ( ! array_key_exists( $value, ocin_categories_ar() ) )
        $value = '';
    return $value;
}

/**
 * Sanitize return an non-negative Integer
 */
function ocin_sanitize_integer( $value ) {
    return absint( $value );
}

/**
 * Sanitize return pro version text
 */
function ocin_pro_version( $input ) {
    return $input;
}

/**
 * Sanitize Any
 */
function ocin_sanitize_any( $input ) {
    return $input;
}

/**
 * Sanitize Text
 */
function ocin_sanitize_text( $str ) {
	return sanitize_text_field( $str );
} 

/**
 * Sanitize Textarea
 */
function ocin_sanitize_textarea( $text ) {
	return esc_textarea( $text );
}

/**
 * Sanitize URL
 */
function ocin_sanitize_url( $url ) {
	return esc_url( $url );
}

/**
 * Sanitize Boolean
 */
function ocin_sanitize_bool( $string ) {
	return (bool)$string;
} 

/**
 * Sanitize Text with html
 */
function ocin_sanitize_text_html( $str ) {
	$args = array(
			    'a' => array(
			        'href' => array(),
			        'title' => array()
			    ),
			    'br' => array(),
			    'em' => array(),
			    'strong' => array(),
			    'span' => array(),
			);
	return wp_kses( $str, $args );
}

/**
 * Sanitize array for multicheck
 * http://stackoverflow.com/a/22007205
 */
function ocin_sanitize_multicheck( $values ) {

    $multi_values = ( ! is_array( $values ) ) ? explode( ',', $values ) : $values;
	return ( ! empty( $multi_values ) ) ? array_map( 'sanitize_title', $multi_values ) : array();
}

/**
 * Sanitize GPS Latitude and Longitud
 * http://stackoverflow.com/a/22007205
 */
function ocin_sanitize_lat_long( $coords ) {
	if ( preg_match( '/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $coords ) ) {
	    return $coords;
	} else {
	    return 'error';
	}
} 



/**
 * Create the "PRO version" buttons
 */
if ( ! function_exists( 'ocin_pro_btns' ) ){
	function ocin_pro_btns( $args ){

		$wp_customize = $args['wp_customize'];
		$title = $args['title'];
		$label = $args['label'];
		if ( isset( $args['priority'] ) || array_key_exists( 'priority', $args ) ) {
			$priority = $args['priority'];
		}else{
			$priority = 120;
		}
		if ( isset( $args['panel'] ) || array_key_exists( 'panel', $args ) ) {
			$panel = $args['panel'];
		}else{
			$panel = '';
		}

		$section_id = sanitize_title( $title );

		$wp_customize->add_section( $section_id , array(
			'title'       => $title,
			'priority'    => $priority,
			'panel' => $panel,
		) );
		$wp_customize->add_setting( $section_id, array(
			'sanitize_callback' => 'ocin_pro_version'
		) );
		$wp_customize->add_control( new ocin_Pro_Version( $wp_customize, $section_id, array(
	        'section' => $section_id,
	        'label' => $label
		   )
		) );
	}
}//end if function_exists

/**
 * Display Text Control
 * Custom Control to display text
 */
if ( class_exists( 'WP_Customize_Control' ) ) {
	class ocin_Display_Text_Control extends WP_Customize_Control {
		/**
		* Render the control's content.
		*/
		public function render_content() {

	        $wp_kses_args = array(
			    'a' => array(
			        'href' => array(),
			        'title' => array(),
			        'data-section' => array(),
			    ),
			    'br' => array(),
			    'em' => array(),
			    'strong' => array(),
			    'span' => array(),
			);
			$label = wp_kses( $this->label, $wp_kses_args );
	        ?>
			<p><?php echo $label; ?></p>		
		<?php
		}
	}
}



/*
* AJAX call to retreive an image URI by its ID
*/
add_action( 'wp_ajax_nopriv_ocin_get_image_src', 'ocin_get_image_src' );
add_action( 'wp_ajax_ocin_get_image_src', 'ocin_get_image_src' );

function ocin_get_image_src() {
	$image_id = $_POST['image_id'];
	$image = wp_get_attachment_image_src( absint( $image_id ), 'full' );
	$image = $image[0];
	echo $image;
	die();
}

<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Ocin
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function ocin_body_classes( $classes ) {

    $ocin_theme_data = wp_get_theme();

    $classes[] = sanitize_title( $ocin_theme_data['Name'] );
    $classes[] = 'v' . $ocin_theme_data['Version'];

    $ocin_slider_fullscreen = get_theme_mod( 'ocin_slider_fullscreen', false );
    if ( class_exists( 'WooCommerce' ) ){
        if ( is_shop() && $ocin_slider_fullscreen || isset( $_GET[ 'fullscreen_slider' ] ) ) {
            $classes[] = 'slider-fullscreen';        
        }
    }

    //Add class for Site Layout
    $ocin_shop_page_layout = get_theme_mod( 'ocin_shop_page_layout', 'shop-narrow' );
    if ( isset( $_GET[ 'shop_page_layout' ] ) ) {
        $ocin_shop_page_layout = sanitize_text_field( wp_unslash( $_GET[ 'shop_page_layout' ] ) );
    }
    $classes[] = 'ocin-' . esc_attr( $ocin_shop_page_layout );

	return $classes;
}
add_filter( 'body_class', 'ocin_body_classes' );


if ( ! function_exists( 'ocin_new_content_more' ) ){
    function ocin_new_content_more($more) {
           global $post;
           return ' <br><a href="' . esc_url( get_permalink() ) . '" class="more-link read-more">' . esc_html__( 'Read more', 'ocin' ) . ' <i class="fa fa-angle-right"></i></a>';
    }   
}// end function_exists
    add_filter( 'the_content_more_link', 'ocin_new_content_more' );


/**
 * Meta Slider configurations
 */
function ocin_metaslider_default_slideshow_properties( $params ) {
        $params['width'] = 1450;
        $params['height'] = 700;
	return $params;
}
add_filter( 'metaslider_default_parameters', 'ocin_metaslider_default_slideshow_properties', 10, 1 );

/**
 * Meta Slider referall ID
 */
function ocin_metaslider_hoplink( $link ) {
    return "https://getdpd.com/cart/hoplink/15318?referrer=24l934xmnt6sc8gs";
    
}
add_filter( 'metaslider_hoplink', 'ocin_metaslider_hoplink', 10, 1 );

/**
 * Retrieve sliders from Meta Slider plugin
 */
function ocin_all_meta_sliders( $sort_key = 'date' ) {

    $sliders = array();

    // list the tabs
    $args = array(
        'post_type' => 'ml-slider',
        'post_status' => 'publish',
        'orderby' => $sort_key,
        'suppress_filters' => 1, // wpml, ignore language filter
        'order' => 'ASC',
        'posts_per_page' => -1
    );

    $args = apply_filters( 'metaslider_all_meta_sliders_args', $args );

    // WP_Query causes issues with other plugins using admin_footer to insert scripts
    // use get_posts instead
    $all_sliders = get_posts( $args );

    foreach( $all_sliders as $slideshow ) {

        $sliders[] = array(
            'title' => $slideshow->post_title,
            'id' => $slideshow->ID
        );

    }

    return $sliders;

}


/**
 * Convert HEX colors to RGB
 */
function hex2rgb( $colour ) {
    $colour = str_replace("#", "", $colour);
    if ( strlen( $colour ) == 6 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
    } elseif ( strlen( $colour ) == 3 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
    } else {
            return false;
    }
    $r = hexdec( $r );
    $g = hexdec( $g );
    $b = hexdec( $b );
    return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}
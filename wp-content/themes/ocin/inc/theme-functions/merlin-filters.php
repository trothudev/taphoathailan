<?php
/**
 * Custom content for the generated child theme's functions.php file.
 *
 * @param string $output Generated content.
 * @param string $slug Parent theme slug.
 */
function ocin_generate_child_functions_php( $output, $slug ) {
	$slug_no_hyphens = strtolower( preg_replace( '#[^a-zA-Z]#', '', $slug ) );
	$output = "
		<?php
		/**
		 * Theme functions and definitions.
		 */
		function {$slug_no_hyphens}_child_enqueue_styles() {
		        wp_enqueue_style( '{$slug}-style' , get_template_directory_uri() . '/style.css', array( 'bootstrap', 'owl-carousel', 'photoswipe', 'photoswipe-skin' ) );
		    wp_enqueue_style( '{$slug}-child-style',
		        get_stylesheet_directory_uri() . '/style.css',
		        array( '{$slug}-style' ),
		        wp_get_theme()->get('Version')
		    );
		}
		add_action(  'wp_enqueue_scripts', '{$slug_no_hyphens}_child_enqueue_styles' );\n
	";
	// Let's remove the tabs so that it displays nicely.
	$output = trim( preg_replace( '/\t+/', '', $output ) );
	// Filterable return.
	return $output;
}
add_filter( 'merlin_generate_child_functions_php', 'ocin_generate_child_functions_php', 10, 2 );

/**
 * Filter the home page title from your demo content.
 * If your demo's home page title is "Home", you don't need this.
 *
 * @param string $output Home page title.
 */
function ocin_merlin_content_home_page_title( $output ) {
	return 'Shop';
}
add_filter( 'merlin_content_home_page_title', 'ocin_merlin_content_home_page_title' );
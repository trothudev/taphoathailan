<?php
/*
 * Retreive WooCommerce items for AJAX
 */
function ocin_load_items(){

	if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] )) {

		$token = $_POST['token'];
		//Comprabar que tenga un token correcto para evitar abuso
		if( wp_verify_nonce( $token, 'quemalabs-secret' ) ){

			$category = esc_attr( $_POST['category'] );
			$offset = intval( esc_attr( $_POST['offset'] ) );
			$product_amout = get_theme_mod( 'ocin_shop_products_amount', '12' );
			$args = array(
				'posts_per_page' => $product_amout,
				'post_type' => 'product',
				'orderby' => 'menu_order title',
				'order'   => 'ASC',
				'post_status' => 'publish'
			);
			if ( 'all' != $category ) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'slug',
						'terms'    => $category,
					),
				);
			}
			if ( $offset ) {
				$args['offset'] = $offset;
				$args['posts_per_page'] = ( $product_amout / 2 );
			}
			// The Query
			$the_query = new WP_Query( $args );

			// The Loop
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					wc_get_template_part( 'content', 'product' );
				}
			} else {
				// no posts found
			}
			/* Restore original Post Data */
			wp_reset_postdata();
			wp_die();

			
		}else{

			echo( 'Ivalid nounce' ); //This do not render to the front-end

		}//end token
		
	} // end IF

}// ocin_load_items()

add_action( 'wp_ajax_nopriv_ocin_load_items', 'ocin_load_items' );
add_action( 'wp_ajax_ocin_load_items', 'ocin_load_items' );



/**
 * Adds product class for ajax
 */
function ocin_add_product_class( $classes ) {

	if( 'product' == get_post_type() ){
		$classes[] = 'product';
	}
	return $classes;
}
add_filter( 'post_class', 'ocin_add_product_class' );
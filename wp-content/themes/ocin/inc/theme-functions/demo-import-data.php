<?php 
/**
 * Set import files
 */
function ocin_import_files() {
    return array(
        // array(
        //     'import_file_name'           => esc_attr__( 'Demo Import 1', 'ocin' ),
        //     'categories'                 => array(),
        //     'import_file_url'            => 'http://www.quemalabs.com/files/import-files/ocin/content.xml',
        //     'import_widget_file_url'     => 'http://www.quemalabs.com/files/import-files/ocin/widgets.wie',
        //     'import_customizer_file_url' => 'http://www.quemalabs.com/files/import-files/ocin/export.dat',
        //     'import_preview_image_url'   => 'http://www.quemalabs.com/files/import-files/ocin/screenshot.png',
        //     'import_notice'              => esc_html__( 'Click on "Import Demo Data" to start importing. Images were replaced by a placeholder to avoid long waits.', 'ocin' ),
        // ),
    );
}
add_filter( 'pt-ocdi/import_files', 'ocin_import_files' );

// Disable Proteus branding
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

/**
 * Assign Menus, Front adn Blog page
 */
function ocin_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Demo', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer', 'nav_menu' );
    $social_menu = get_term_by( 'name', 'Social', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer-menu' => $footer_menu->term_id,
            'social' => $social_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Shop' );
    $shop_page_id = get_page_by_title( 'Shop' );
    $blog_page_id  = get_page_by_title( 'Blog' );
    $woocommerce_cart_page_id = get_page_by_title( 'Cart' );
    $woocommerce_checkout_page_id = get_page_by_title( 'Checkout' );
    $woocommerce_myaccount_page_id = get_page_by_title( 'My Account' );

    update_option( 'show_on_front', 'page' );
    
    //Front Page and Blog Page
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

    //WooCommerce
    update_option( 'woocommerce_shop_page_id', $shop_page_id->ID );
    update_option( 'woocommerce_cart_page_id', $woocommerce_cart_page_id->ID );
    update_option( 'woocommerce_checkout_page_id', $woocommerce_checkout_page_id->ID );
    update_option( 'woocommerce_myaccount_page_id', $woocommerce_myaccount_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'ocin_after_import_setup' );

<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
?>
<?php
$shop_layout = get_theme_mod( 'ocin_shop_layout', 'grid' );
$ocin_shop_infinitescroll_enable = get_theme_mod( 'ocin_shop_infinitescroll_enable', true );
$shop_infinite = ( $ocin_shop_infinitescroll_enable ) ? 'infinite-scroll' : '';
$shop_columns = get_theme_mod( 'ocin_shop_columns', '4' );
//For demo only
if ( isset( $_GET[ 'shop_layout' ] ) ) {
	$shop_columns = esc_attr( $_GET[ 'shop_layout' ] );
}

$product_class = $shop_layout . ' ' . 'layout-' . $shop_columns . '-columns ' . $shop_infinite;
?>
<ul class="products <?php echo esc_attr( $product_class ); ?>">

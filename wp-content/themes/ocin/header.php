<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ocin
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- WP_Head -->
<?php wp_head(); ?>
<!-- End WP_Head -->

</head>

<body <?php body_class(); ?>>
<div class="ocin_loader"><div class="spinner"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>
<?php
$ocin_topbar_enable = get_theme_mod( 'ocin_topbar_enable', true );
if ( $ocin_topbar_enable || is_customize_preview() || isset( $_GET[ 'top_bar' ] ) ) :
?>
<div class="top-bar <?php if( false == $ocin_topbar_enable && empty( $_GET[ 'top_bar' ] ) ): echo 'hidden'; endif ?>" >
    <div class="container">
        <div class="row">
        <?php
        $ocin_top_bar_class = 'col-md-12 col-sm-12';
        if ( has_nav_menu( 'top-bar-menu' ) ) {
            $ocin_top_bar_class = 'col-md-6 col-sm-6';
        }
        ?>
            <div class="<?php echo esc_attr( $ocin_top_bar_class ); ?>">
                <?php $ocin_topbar_text = get_theme_mod( 'ocin_topbar_text', '' ); ?>
                <p><?php echo wp_kses_post( $ocin_topbar_text ); ?></p>
            </div>
            <?php
                if ( has_nav_menu( 'top-bar-menu' ) ) {
            ?>
             <div class="col-md-6 col-sm-6 align-right">
                <?php
                $args = array(
                        'theme_location'  => 'top-bar-menu',
                        'container'       => 'div',
                        'container_id'    => 'top-bar-menu',
                        'container_class' => 'menu',
                        'menu_id'         => 'menu-top-bar-items',
                        'menu_class'      => 'menu-items',
                        'depth'           => 1,
                        'fallback_cb'     => '',
                    );                    
                wp_nav_menu( $args );
                ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div><!-- .top-bar -->
<?php endif ?>

    <?php
    $header_image = "";
    if ( get_header_image() ){
        $header_image = get_header_image();
    }
    $ocin_menu_class = 'col-md-5 col-md-pull-2 col-sm-6';
    $ocin_logo_class = 'col-md-2 col-md-push-5';
    $ocin_cart_class = 'col-md-5 col-sm-6';
    $ocin_header_class = 'default';
    $ocin_header_layout = get_theme_mod( 'ocin_header_layout', 'default' );

    $header_par = isset( $_GET[ 'header' ] ) ? $_GET[ 'header' ] : '';
    if ( 'menu-center' == $ocin_header_layout || 'menu-center' == $header_par ) {
        $ocin_menu_class = 'col-md-6 col-sm-6';
        $ocin_logo_class = 'col-md-3';
        $ocin_cart_class = 'col-md-3 col-sm-6';
        $ocin_header_class = 'menu-center';
    }else if ( 'minimal' == $ocin_header_layout || 'minimal' == $header_par ) {
        $ocin_menu_class = 'hidden visible-xs';
        $ocin_logo_class = 'col-md-4 col-md-push-2 col-sm-6';
        $ocin_cart_class = 'hidden visible-xs';
        $ocin_header_class = 'minimal';
    }
    ?>
	<header id="header" class="site-header <?php echo esc_attr( $ocin_header_class ); ?>" role="banner" <?php echo ( $header_image ) ? 'style="background-image: url(' . esc_url( $header_image ) . ');"' : ''; ?>>
		<div class="container">
        	<div class="row">

                <?php if( 'minimal' == $ocin_header_layout || 'minimal' == $header_par ){ ?>
                <div class="col-md-2 col-sm-3 hidden-xs"><a  class="minimal-btn" href="#"><i class="fa fa-navicon"></i><i class="fa fa-close"></i></a></div>
                <?php }; ?>


                <div class="logo_container <?php echo esc_attr( $ocin_logo_class ); ?>">
                    <?php
                    $logo = wp_get_attachment_image_src( absint( get_theme_mod( 'ocin_logo' ) ), 'full' );
                    $logo = $logo[0];
                    ?>
                    <?php if ( is_front_page() || is_home() ) : ?>
                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="ql_logo"><?php if ( !empty( $logo ) ) : echo '<img src="' . esc_url( $logo ) . '" />'; else: bloginfo( 'name' ); endif; ?></a></h1>
                    <?php else : ?>
                        <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="ql_logo"><?php if ( !empty( $logo ) ) : echo '<img src="' . esc_url( $logo ) . '" />'; else: bloginfo( 'name' ); endif; ?></a></p>
                    <?php endif; ?>

                    <button id="ql_nav_btn" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ql_nav_collapse" aria-expanded="false">
                        <i class="fa fa-navicon"></i>
                    </button>
                    
                </div><!-- /logo_container -->



        		<div class="<?php echo esc_attr( $ocin_menu_class ); ?>">

                    <div class="collapse navbar-collapse" id="ql_nav_collapse">
                        <nav id="jqueryslidemenu" class="jqueryslidemenu navbar " role="navigation">
                            <?php
                            wp_nav_menu( array(                     
                                'theme_location'  => 'primary',
                                'menu_id' => 'primary-menu',
                                'depth'             => 3,
                                'menu_class'        => 'nav',
                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'walker'            => new wp_bootstrap_navwalker()
                            ));
                            ?>
                        </nav>
                    </div><!-- /ql_nav_collapse -->

                </div><!-- col-md-5 -->
               

                
                <?php 
                if ( class_exists( 'woocommerce' ) ) {  
                ?>
                    <div class="<?php echo esc_attr( $ocin_cart_class ); ?>">
                        
                        <div class="ql_cart_wrap">
                            <button href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" class="ql_cart-btn"><?php echo wp_kses_post( WC()->cart->get_cart_total() ); ?> <span class="count">(<?php echo esc_html( WC()->cart->cart_contents_count ); ?>)</span> <i class="ql-bag"></i><i class="ql-chevron-down"></i></button>



                            <div id="ql_woo_cart">
                                <?php global $woocommerce; ?>
                                
                                <?php the_widget( 'WC_Widget_Cart' );  ?>
                            </div><!-- /ql_woo_cart --> 
                        </div>

                        <div class="login_btn_wrap">
                            <?php if ( is_user_logged_in() ) { ?>
                                <a class="ql_login-btn" href="<?php echo esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ); ?>" title="<?php esc_attr_e( 'Tài khoản', 'ocin' ); ?>"><?php esc_html_e( 'Tài khoản', 'ocin' ); ?></a>
                             <?php } 
                             else { ?>
                                <a class="ql_login-btn" href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>" title="<?php esc_attr_e( 'Đăng nhập', 'ocin' ); ?>"><?php esc_html_e( 'Đăng nhập', 'ocin' ); ?></a>
                             <?php } ?>
                        </div><!-- /login_btn_wrap -->

                        <div class="clearfix"></div>
                    </div><!-- col-md-5 col-md-offset-2 -->
                <?php } //if WooCommerce active ?>





                <div class="clearfix"></div>
                
                <?php if( 'minimal' == $ocin_header_layout || 'minimal' == $header_par ){ ?>

                    <div class="bottom-menu">
                        

                        <div class="col-md-8 col-sm-6">

                            <div class="collapse navbar-collapse" id="ql_nav_collapse">
                                <nav id="jqueryslidemenu" class="jqueryslidemenu navbar " role="navigation">
                                    <?php
                                    wp_nav_menu( array(                     
                                        'theme_location'  => 'primary',
                                        'menu_id' => 'primary-menu',
                                        'depth'             => 3,
                                        'menu_class'        => 'nav',
                                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                        'walker'            => new wp_bootstrap_navwalker()
                                    ));
                                    ?>
                                </nav>
                            </div><!-- /ql_nav_collapse -->

                        </div><!-- col-md-5 -->

                        <?php 
                        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { 
                        ?>
                            <div class="col-md-4 col-sm-6">
                                
                                <div class="ql_cart_wrap">
                                    <button href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" class="ql_cart-btn"><?php echo wp_kses_post( WC()->cart->get_cart_total() ); ?> <span class="count">(<?php echo esc_html( WC()->cart->cart_contents_count ); ?>)</span> <i class="ql-bag"></i><i class="ql-chevron-down"></i></button>



                                    <div id="ql_woo_cart">
                                        <?php global $woocommerce; ?>
                                        
                                        <?php the_widget( 'WC_Widget_Cart' );  ?>
                                    </div><!-- /ql_woo_cart --> 
                                </div>

                                <div class="login_btn_wrap">
                                    <?php if ( is_user_logged_in() ) { ?>
                                        <a class="ql_login-btn" href="<?php echo esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ); ?>" title="<?php esc_attr_e( 'My Account', 'ocin' ); ?>"><?php esc_html_e( 'My Account', 'ocin' ); ?></a>
                                     <?php } 
                                     else { ?>
                                        <a class="ql_login-btn" href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>" title="<?php esc_attr_e( 'Login', 'ocin' ); ?>"><?php esc_html_e( 'Login', 'ocin' ); ?></a>
                                     <?php } ?>
                                </div><!-- /login_btn_wrap -->

                                <div class="clearfix"></div>
                            </div><!-- col-md-5 col-md-offset-2 -->
                        <?php } //if WooCommerce active ?>

                    </div><!-- /bottom-menu -->

                <?php } //if menu minimal ?>

        	</div><!-- row-->
        </div><!-- /container -->
	</header>
    
	<div class="clearfix"></div>


    <?php
    $ocin_slider_id = get_theme_mod( 'ocin_slider_id', 'no' );
    $ocin_slider_fullscreen = get_theme_mod( 'ocin_slider_fullscreen', false );
    if ( class_exists( 'WooCommerce' ) ){
        if ( is_shop() && true == $ocin_slider_fullscreen && 'no' != $ocin_slider_id || isset( $_GET[ 'fullscreen_slider' ] ) ) {
            //Demo only
            if ( isset( $_GET[ 'fullscreen_slider' ] ) ) : $ocin_slider_id = '273'; endif;
        ?>
            <div class="ocin_fullscreen_slider">
                    <?php echo do_shortcode( '[metaslider id=' . $ocin_slider_id . ']' ); ?>
            </div><!-- .ocin_fullscreen_slider -->
        <?php 
        } 
    }
    ?>




    <?php 
    $ocin_blog_layout = get_theme_mod( 'ocin_blog_layout', 'masonry' );
    $ocin_shop_page_layout = get_theme_mod( 'ocin_shop_page_layout', 'shop-narrow' );
    if ( isset( $_GET[ 'shop_page_layout' ] ) ) {
        $ocin_shop_page_layout = sanitize_text_field( wp_unslash( $_GET[ 'shop_page_layout' ] ) );
    }
    $container_class = 'container';
    $row_class = 'row';
    if (  is_home() && 'masonry' == $ocin_blog_layout && ! isset( $_GET[ 'blog_layout' ] ) ) {
        $container_class = '';
        $row_class = '';
    }
    if ( function_exists( 'is_shop' ) ) {
        if ( is_shop() || is_product_category() ) {
            if ( 'shop-fullwidth' == $ocin_shop_page_layout ) {
                $container_class = 'container-fluid';
            }
        }
    }

    ?>
    
    <div id="container" class="<?php echo esc_attr( $container_class ); ?>">

        <main id="main" class="site-main <?php echo esc_attr( $row_class ); ?>" role="main">
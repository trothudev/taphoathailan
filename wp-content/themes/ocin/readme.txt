=== Ocin ===

Ocin is a beautiful WooCommerce theme perfect for modern and clean online shops. With a responsive design will make your online store to look great on all devices.

Author: Quema Labs
Tags: light, white, gray, right-sidebar, two-columns, responsive-layout, custom-background, custom-colors, custom-header, custom-menu, featured-image-header, featured-images, flexible-header, full-width-template, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, photoblogging


License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html


== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


= Documentation =

* Social Menu
Using the social nav menu is just like using any other nav menu in WordPress. You just need to add custom links pointing to your social media profiles.

So, the first step would be to create a new menu under the “Appearance > Menus” screen in the WordPress admin. To add custom links to this menu, use the “Links” box. Just make a link to the social media profiles you want to display.

And, for the last part, be sure to select “Social” for the “Theme Location” before saving your menu.


* How to get the PRO version of Ocin
The PRO version is called "Ocin" and includes tons of more features. You can buy it here:
https://www.quemalabs.com/theme/ocin/


== Credits ==

Ocin WordPress Theme, Copyright 2016 Nicolas Andrade
Ocin is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Ocin WordPress Theme bundles the following third-party resources:

Underscores 
(C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
Source: http://underscores.me/

Font Awesome icon font, Created by Dave Gandy
Font Awesome licensed under SIL OFL 1.1 · Code licensed under MIT License
Source: http://fortawesome.github.io/

Bootstrap Created by @mdo and @fat.
Code licensed under MIT
Source: http://getbootstrap.com

Isotope by Metafizzy | GPLv3
License: http://isotope.metafizzy.co/license.html
Source: http://isotope.metafizzy.co/

imagesLoaded by Metafizzy | MIT License
License: http://desandro.mit-license.org/
Source: http://imagesloaded.desandro.com

Packery by Metafizzy | GPLv3
License: http://packery.metafizzy.co/license.html
Source: http://packery.metafizzy.co/

Owl Carousel 2 | MIT License
License: https://raw.githubusercontent.com/smashingboxes/OwlCarousel2/develop/LICENSE
Source: http://www.owlcarousel.owlgraphic.com

PhotoSwipe by @dimsemenov | MIT License
License: https://raw.githubusercontent.com/dimsemenov/PhotoSwipe/master/LICENSE
Source: http://photoswipe.com

Pace
License: https://raw.githubusercontent.com/HubSpot/pace/master/LICENSE
Source: http://github.hubspot.com/pace/docs/welcome/

HTML5 Shiv by @afarkas @jdalton @jon_neal @rem
License: MIT/GPL2 Licensed
Source: https://github.com/afarkas/html5shiv

[Lato Font](https://www.google.com/fonts/specimen/Lato) by Łukasz Dziedzic - Licensed under the [SIL Open Font License, 1.1](http://scripts.sil.org/OFL).

Images from Unsplash
License: https://unsplash.com/license
Source: https://unsplash.com

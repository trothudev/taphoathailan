					<div class="clearfix"></div>
					<?php
		            if ( ! is_shop() && ! is_product() && ! is_product_category() && ! is_product_tag() ) {
		                echo '</div><!-- /ql_background -->';
		            }
		            ?>
				
        	</div><!-- /content -->

        	<?php
        	/**
             * woocommerce_sidebar hook
             *
             * @hooked woocommerce_get_sidebar - 10
             */
            do_action( 'woocommerce_sidebar' );
        	?>

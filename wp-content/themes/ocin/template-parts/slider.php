<?php
            $ocin_slider_id = get_theme_mod( 'ocin_slider_id', 'no' );
            $ocin_slider_fullscreen = get_theme_mod( 'ocin_slider_fullscreen', false );
            if ( 'no' != $ocin_slider_id && false == $ocin_slider_fullscreen && is_shop() || isset( $_GET[ 'shop_slider' ] )  ) {
            ?>
                <div class="welcome-section">
                    <div class="row">

                        <?php
                        $banner_1['image'] = wp_get_attachment_image_src( absint( get_theme_mod( 'ocin_banner1_image' ) ), 'full' );
                        $banner_1['image'] = $banner_1['image'][0];
                        $banner_1['title'] = get_theme_mod( 'ocin_banner1_title', esc_html__( 'Your Title', 'ocin' ) );
                        $banner_1['text'] = get_theme_mod( 'ocin_banner1_text', esc_html__( 'Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.', 'ocin' ) );
                        $banner_1['link_title'] = get_theme_mod( 'ocin_banner1_link_title', esc_html__( 'View More', 'ocin' ) );
                        $banner_1['link_url'] = get_theme_mod( 'ocin_banner1_link_url', '#' );
                        $banner_1['color'] = hex2rgb( get_theme_mod( 'ocin_banner1_background_color', '' ) );
                        $banner_1['enable'] = get_theme_mod( 'ocin_banner1_enable', true );

                        $banner_2['image'] = wp_get_attachment_image_src( absint( get_theme_mod( 'ocin_banner2_image' ) ), 'full' );
                        $banner_2['image'] = $banner_2['image'][0];
                        $banner_2['title'] = get_theme_mod( 'ocin_banner2_title', esc_html__( 'Your Title', 'ocin' ) );
                        $banner_2['text'] = get_theme_mod( 'ocin_banner2_text', esc_html__( 'Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.', 'ocin' ) );
                        $banner_2['link_title'] = get_theme_mod( 'ocin_banner2_link_title', esc_html__( 'View More', 'ocin' ) );
                        $banner_2['link_url'] = get_theme_mod( 'ocin_banner2_link_url', '#' );
                        $banner_2['color'] = hex2rgb( get_theme_mod( 'ocin_banner2_background_color', '' ) );
                        $banner_2['enable'] = get_theme_mod( 'ocin_banner2_enable', true );
                        ?>

                        <div class="<?php echo ( false == $banner_1['enable'] && false == $banner_2['enable'] ? 'col-md-12' : 'col-md-9' ); ?>">
                            <div class="theme-slider">
                                <?php
                                if ( is_shop() && 'no' != $ocin_slider_id || isset( $_GET[ 'shop_slider' ] ) ) {
                                    //Demo only
                                    if ( isset( $_GET[ 'shop_slider' ] ) ) : $ocin_slider_id = '160'; endif;

                                    echo do_shortcode( '[metaslider id=' . $ocin_slider_id . ']' ); 
                                }
                                ?>
                            </div>
                        </div><!-- .col-md-9 -->
                        

                        <?php if ( true == $banner_1['enable'] || true == $banner_2['enable'] ) { ?>
                            <div class="col-md-3">
                                <?php if ( true == $banner_1['enable'] ) { ?>
                                    <div class="welcome-banner banner-top">
                                        <div href="#" class="banner-image" style="<?php echo ( ! empty( $banner_1['image'] ) ? 'background-image: url(' . $banner_1['image'] . ');' : '' ); ?> "></div>
                                        <div class="banner-content">
                                            <h4><?php echo esc_html( $banner_1['title'] ); ?></h4>
                                            <p><?php echo esc_html( $banner_1['text'] ); ?></p>
                                            <a href="<?php echo esc_url( $banner_1['link_url'] ); ?>" class="woocommerce_btn"><?php echo esc_html( $banner_1['link_title'] ); ?></a>
                                        </div>
                                        <a href="<?php echo esc_url( $banner_1['link_url'] ); ?>" class="banner-link"></a>
                                    </div>
                                    <?php if ( $banner_1['color'] ) { ?>
                                        <style>
                                        .welcome-banner.banner-top .banner-image::before{
                                            background-color: rgba(<?php echo $banner_1['color']['red'] . ', ' . $banner_1['color']['green'] . ', ' . $banner_1['color']['blue']; ?>, 0.8);
                                        }
                                        </style>
                                    <?php } ?>
                                <?php } ?>
                                
                                <?php if ( true == $banner_2['enable'] ) { ?>
                                    <div class="welcome-banner banner-bottom">
                                        <div href="#" class="banner-image" style="<?php echo ( ! empty( $banner_2['image'] ) ? 'background-image: url(' . $banner_2['image'] . ');' : '' ); ?> "></div>
                                        <div class="banner-content">
                                            <h4><?php echo esc_html( $banner_2['title'] ); ?></h4>
                                            <p><?php echo esc_html( $banner_2['text'] ); ?></p>
                                            <a href="<?php echo esc_url( $banner_2['link_url'] ); ?>" class="woocommerce_btn"><?php echo esc_html( $banner_2['link_title'] ); ?></a>
                                        </div>
                                        <a href="<?php echo esc_url( $banner_2['link_url'] ); ?>" class="banner-link"></a>
                                    </div>
                                    <?php if ( $banner_2['color'] ) { ?>
                                        <style>
                                        .welcome-banner.banner-bottom .banner-image::before{
                                            background-color: rgba(<?php echo $banner_2['color']['red'] . ', ' . $banner_2['color']['green'] . ', ' . $banner_2['color']['blue']; ?>, 0.8);
                                        }
                                        </style>
                                    <?php } ?>
                                <?php } ?>

                            </div><!-- .col-md-3 -->
                        <?php } ?>
                    </div><!-- .row -->
                </div><!-- .welcome-section -->
            <?php }//if no slider ?>
        <?php 
        $ocin_shop_sidebar_position = get_theme_mod( 'ocin_shop_sidebar_position', 'top' );
        if ( 'aside' == $ocin_shop_sidebar_position || isset( $_GET[ 'sidebar_side' ] ) ) {
            $ocin_content_class = 'col-md-9 col-md-push-3';
        }else{
            $ocin_content_class = 'col-md-12';
        }
        ?>
        <div id="content" class="<?php echo $ocin_content_class; ?>">
            <?php
            if ( ! is_shop() && ! is_product() && ! is_product_category() && ! is_product_tag() ) {
                echo '<div class="ql_background_padding">';
            }
            ?>
            

            <?php get_template_part( 'template-parts/slider' ); ?>

        
            <?php
            if ( ! is_shop() ) {
                echo '<header class="page-header">';
                the_archive_title( '<h1 class="page-title">', '</h1>' );
                echo '</header><!-- .page-header -->';
            }
            ?>

            <?php if ( get_search_query() ) { ?>
                <header class="page-header">
                    <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'ocin' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                </header><!-- .page-header -->
            <?php } ?>


            <div class="ql_woocommerce_info">
                <div class="row">
                    <div class="col-md-8">
                        <div class="ql_woocommerce_categories">
                            <ul>
                                <?php
                                $current_class = is_shop() ? "current" : "";
                                echo '<li class="' .  $current_class . '"><a href="' . esc_url( get_permalink( wc_get_page_id( 'shop' ) ) ) . '" data-category="all">' . esc_html__( 'Tất cả sản phẩm', 'ocin' ) . '</a></li>';

                                $ocin_shop_categories = get_theme_mod( 'ocin_shop_categories', '' );
                                
                                if ( is_array( $ocin_shop_categories ) ) {
                                    
                                    foreach ( $ocin_shop_categories as $slug ) {

                                        $term = get_term_by( 'slug', $slug, 'product_cat' );

                                        // The $term is an object, so we don't need to specify the $taxonomy.
                                        $term_link = get_term_link( $slug, 'product_cat' );

                                        // If there was an error, continue to the next term.
                                        if ( is_wp_error( $term_link ) ) {
                                            continue;
                                        }
                                        $current_cat = is_product_category( $slug ) ? 'current' : '';
                                        // We successfully got a link. Print it out.
                                        echo '<li class="' . $current_cat . '"><a href="' . esc_url( $term_link ) . '" data-category="' . esc_attr( $slug ) . '">' . esc_html( $term->name ) . '</a></li>';
                                    }                                
                                }

                                ?>
                                <li class="ql_product_search"><i class="ql-magnifier"></i>
                                <?php get_product_search_form( true ); ?>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <?php if ( is_active_sidebar( 'shop-sidebar' ) && 'top' == $ocin_shop_sidebar_position && ! isset( $_GET[ 'sidebar_side' ] ) ) {  ?>
                        <a class="sidebar_btn" href="#"><i class="ql-funnel"></i><?php esc_html_e( 'Filter', 'ocin' ); ?></a>
                        <?php } ?>
                    </div>
                </div><!-- /row -->
            </div><!-- /woocommerce_info -->


            <?php
                if ( 'top' == $ocin_shop_sidebar_position && ! isset( $_GET[ 'sidebar_side' ] ) ) {
                    /**
                     * woocommerce_sidebar hook
                     *
                     * @hooked woocommerce_get_sidebar - 10
                     */
                    do_action( 'woocommerce_sidebar' );
                }
                
            ?>
		